package com.desiratech.rssfeedsassignment_final;

/**
 * Created by David on 4/20/2015.
 */
public class Article {

    private String strTitle, strDescription, strPubDate, strLink;

    public void setTitle(String title){
        strTitle = title;
    }
    public void setDescription(String description){
        strDescription = description;
    }
    public void setPubDate(String pubDate){
        strPubDate = pubDate;
    }
    public void setLink(String link){
        strLink = link;
    }

    public String getStrTitle(){
        return strTitle;
    }

    public String getStrDescription(){
        return strDescription;
    }

    public String getStrPubDate(){
        return strPubDate;
    }

    public String getStrLink(){
        return strLink;
    }
}

