package com.desiratech.rssfeedsassignment_final;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 4/19/2015.
 */
public class ArticleAdapter extends BaseAdapter{
    Context mContext;
    List<Article> articlesArrayList;
    String currentDefaultFeed, currentFontSize;

    LayoutInflater layoutInflater;

    public ArticleAdapter(Context context, List<Article> articles){
        mContext = context;
        // Log.d("Is this the activity name?", context.toString());
        articlesArrayList = articles;
        layoutInflater = LayoutInflater.from(context);
    }
    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }



    @Override
    public int getCount() {
        return articlesArrayList.size();
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String strActivityName = mContext.getClass().getSimpleName();
        boolean showDescriptionAndLink = false;
        ViewHolderArticles vh;

        if (strActivityName == "ShowFullDetails") {
            showDescriptionAndLink = true;
        }
        if(convertView == null){
            vh = new ViewHolderArticles();
            convertView = layoutInflater.inflate(R.layout.list_item,parent,false);

            vh.tv1 = (TextView) convertView.findViewById(R.id.lblTitle);
            vh.tv2 = (TextView) convertView.findViewById(R.id.lblPubDate);


            vh.tv3 = (TextView) convertView.findViewById(R.id.lblDescription);
            vh.tv4 = (TextView) convertView.findViewById(R.id.lblLink);

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);

            if(currentDefaultFeed == null){
                currentDefaultFeed = sharedPreferences.getString("pref_defaultFeed", "%2Flocal");

            }
            if(currentFontSize == null){
                currentFontSize = sharedPreferences.getString("pref_fontSize", "Medium");

            }

            if(currentFontSize != null && currentFontSize != ""){
                switch(currentFontSize){
                    case "Small":
                        vh.tv1.setTextSize(9);
                        vh.tv2.setTextSize(9);
                        vh.tv3.setTextSize(9);
                        vh.tv4.setTextSize(9);
                        break;

                    case "Medium":

                        vh.tv1.setTextSize(15);
                        vh.tv2.setTextSize(15);
                        vh.tv3.setTextSize(15);
                        vh.tv4.setTextSize(15);
                        break;
                    case "Large":

                        vh.tv1.setTextSize(25);
                        vh.tv2.setTextSize(25);
                        vh.tv3.setTextSize(25);
                        vh.tv4.setTextSize(25);
                        break;


                }
            }

            convertView.setTag(vh);
        }else {
            vh = (ViewHolderArticles) convertView.getTag();
        }

        Article objArticle  = articlesArrayList.get(position);

        vh.tv1.setText(objArticle.getStrTitle());
        vh.tv2.setText(objArticle.getStrPubDate());
        vh.tv3.setText(objArticle.getStrDescription());
        vh.tv4.setText(objArticle.getStrLink());
        return convertView;
    }

    public class ViewHolderArticles {
        TextView tv1, tv2, tv3, tv4;
    }


}
