package com.desiratech.rssfeedsassignment_final;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by David on 2/24/2015.
 * Inspired By http://stackoverflow.com/questions/18504615/how-to-display-parsed-xml-data-in-to-listview-in-android
 */
//
class CustomAdapter extends BaseAdapter {
    Context mContext;
    ArrayList<SAXHandler.Entry> entriesArrayList;
    LayoutInflater layoutInflater;
    String currentFontColor, currentBgColor, currentFontSize;

    public CustomAdapter(Context context, ArrayList<SAXHandler.Entry> entries){
        mContext = context;
       // Log.d("Is this the activity name?", context.toString());
        entriesArrayList = entries;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return entriesArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String strActivityName = mContext.getClass().getSimpleName();
        boolean showDescriptionAndLink = false;
        //Log.d("class name", strActivityName);
        ViewHolder vh;

        if (strActivityName == "ShowFullDetails"){
            showDescriptionAndLink = true;
        }
        if(convertView == null){
            vh = new ViewHolder();
            convertView = layoutInflater.inflate(R.layout.list_item,parent,false);

            vh.tv1 = (TextView) convertView.findViewById(R.id.lblTitle);
            vh.tv2 = (TextView) convertView.findViewById(R.id.lblPubDate);


                vh.tv3 = (TextView) convertView.findViewById(R.id.lblDescription);
                vh.tv4 = (TextView) convertView.findViewById(R.id.lblLink);

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
            if(currentBgColor == null){
                currentBgColor = sharedPreferences.getString("pref_theme", "black");

            }
            if(currentFontColor == null){
                currentFontColor = sharedPreferences.getString("pref_defaultFeed", "white");

            }
            if(currentFontSize == null){
                currentFontSize = sharedPreferences.getString("pref_fontSize", "Medium");

            }

            if(currentFontSize != null && currentFontSize != ""){
                switch(currentFontSize){
                    case "Small":
                        vh.tv1.setTextSize(11);
                        vh.tv2.setTextSize(8);
                        vh.tv3.setTextSize(9);
                        vh.tv4.setTextSize(9);
                        break;

                    case "Medium":

                        vh.tv1.setTextSize(17);
                        vh.tv2.setTextSize(12);
                        vh.tv3.setTextSize(15);
                        vh.tv4.setTextSize(15);
                        break;
                    case "Large":

                        vh.tv1.setTextSize(21);
                        vh.tv2.setTextSize(17);
                        vh.tv3.setTextSize(19);
                        vh.tv4.setTextSize(19);
                        break;


                }
            }

            convertView.setTag(vh);
        }else {
            vh = (ViewHolder) convertView.getTag();
        }

        SAXHandler.Entry objEntry = entriesArrayList.get(position);

        vh.tv1.setText(objEntry.getStrTitle());
        vh.tv1.setTypeface(null, Typeface.BOLD);
        vh.tv2.setText(objEntry.getStrPubDate());



            vh.tv3.setText(objEntry.getStrDescription());
            vh.tv4.setText(objEntry.getStrLink());



        return convertView;


    }

    public class ViewHolder
    {
        TextView tv1, tv2, tv3, tv4;
    }


}


