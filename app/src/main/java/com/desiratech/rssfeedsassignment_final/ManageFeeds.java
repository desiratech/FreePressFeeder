package com.desiratech.rssfeedsassignment_final;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.parse.FindCallback;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;


public class ManageFeeds extends Activity {
    List<ParseObject> ob ;
    ListView listView;
    ProgressDialog mProgressDialog;
    ArticleAdapter adapter;
    private List<Article> articleList = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manage_feeds);

        new RemoteDataTask().execute();
        listView = (ListView) findViewById(R.id.savedArticleList);
       /* listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                sendEntry(view);

            }
        });*/


    }


    private class RemoteDataTask extends AsyncTask<Void, Void, Void>{
        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            mProgressDialog = new ProgressDialog(ManageFeeds.this);

            mProgressDialog.setTitle("Saved Articles");
            mProgressDialog.setMessage("Loading...");
            mProgressDialog.setIndeterminate(false);

            mProgressDialog.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            articleList = new ArrayList<Article>();
            try{
                ParseQuery<ParseObject> query = new ParseQuery<ParseObject>("RSS_Articles");
                query.whereEqualTo("user", ParseUser.getCurrentUser());
                ob = query.find();

                for(ParseObject articles :ob ){
                    Article article = new Article();
                    article.setTitle((String) articles.get("title"));
                    article.setPubDate((String) articles.get("pubDate"));
                    article.setDescription((String) articles.get("description"));
                    article.setLink((String) articles.get("link"));
                    articleList.add(article);
                }
            }
            catch (Exception e){
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }

            return null;

        }

        @Override
        protected void onPostExecute(Void aVoid) {

            adapter = new ArticleAdapter (ManageFeeds.this, articleList);

            listView.setAdapter(adapter);

            mProgressDialog.dismiss();
        }
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_manage_feeds, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void sendEntry(View view){

        Intent intent = new Intent(this, ShowFullDetails.class);

        TextView txtTitle = (TextView) view.findViewById(R.id.lblTitle);
        TextView txtDescription = (TextView) view.findViewById(R.id.lblDescription);
        TextView txtPubDate = (TextView) view.findViewById(R.id.lblPubDate);
        TextView txtLink = (TextView) view.findViewById(R.id.lblLink);


        String strTitle = txtTitle.getText().toString();
        //Log.d("title", strTitle);

        String strDescription = txtDescription.getText().toString();
        String strPubDate = txtPubDate.getText().toString();
        String strLink = txtLink.getText().toString();

        intent.putExtra(Welcome.TITLE, strTitle);
        intent.putExtra(Welcome.DESCRIPTION, strDescription);
        intent.putExtra(Welcome.PUBDATE,strPubDate);
        intent.putExtra(Welcome.LINK,strLink);

        startActivity(intent);
    }


}
