package com.desiratech.rssfeedsassignment_final;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by David on 2/23/2015.
 */
public class SAXHandler extends DefaultHandler {
    private String tempVal, locationVal, conditionVal, tmpValue;
    ArrayList<Entry> arEntries = new ArrayList<Entry>();
    private Entry tmpEntry = new Entry();
    private StringBuilder stringBuilder = new StringBuilder();
   /* public void endElement(String uri, String localName, String qName, Attributes attrs) throws SAXParseException, SAXException {
        if (qName.equals("yweather:condition")) {
            for (int i=0; i<attrs.getLength(); i++) {
                if (attrs.getQName(i).equals("temp")) {
                    tempVal = attrs.getQName(i);
                } else if (attrs.getQName(i).equals("text")) {
                    conditionVal = attrs.getValue(i);
                }
            }
        } else if (qName.equals("yweather:location")) {
            for (int i=0; i<attrs.getLength(); i++) {
                if (attrs.getQName(i).equals("city")) {
                    locationVal = attrs.getValue(i);
                }
            }
        }
    }
*/  public SAXHandler(){



      //  entryList = new ArrayList<Entry>();
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if(qName.equalsIgnoreCase("item")){

            tmpValue = "";

        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {

        super.endElement(uri, localName, qName);
        if(qName.equalsIgnoreCase("item")){
            arEntries.add(tmpEntry);
            tmpEntry = new Entry();

        }

        if(qName.equalsIgnoreCase("title")){
          //  arTitles.add(tmpValue);
             tmpEntry.setStrTitle(tmpValue);
           // Log.d("Title", tmpValue.toString());
        }
        if(qName.equalsIgnoreCase("description")){
            tmpEntry.setStrDescription(tmpValue);
           // Log.d("description", tmpValue.toString());
          // arDescriptions.add(tmpValue);
        }
        if(qName.equalsIgnoreCase("link")){
          //  arLinks.add(tmpValue);
            tmpEntry.setStrLink(tmpValue);
            //Log.d("link", tmpValue.toString());
        }
        if(qName.equalsIgnoreCase("pubDate")){
           // arPubDates.add(tmpValue);
           tmpEntry.setStrPubDate(tmpValue);
           // Log.d("pub", tmpValue.toString());
        }


    }


    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        tmpValue = new String(ch, start, length);
    }


    public ArrayList<Entry> getArEntries(){
        return arEntries;
    }



    public class Entry {
        private String strTitle, strDescription, strPubDate, strLink;

        public void setStrTitle(String title){
            strTitle = title;
        }
        public void setStrDescription(String description){
            strDescription = description;
        }
        public void setStrPubDate(String pubDate){
            strPubDate = pubDate;
        }
        public void setStrLink(String link){
            strLink = link;
        }

        public String getStrTitle(){
            return strTitle;
        }

        public String getStrDescription(){
            return strDescription;
        }

        public String getStrPubDate(){
            return strPubDate;
        }

        public String getStrLink(){
            return strLink;
        }
    }

}