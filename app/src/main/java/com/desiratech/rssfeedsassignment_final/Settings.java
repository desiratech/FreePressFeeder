package com.desiratech.rssfeedsassignment_final;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.PreferenceActivity;
import android.preference.PreferenceFragment;
import android.preference.PreferenceManager;
import android.preference.PreferenceScreen;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;


public class Settings extends Activity {
    private static final String[] FEED_URL = {"http://www.winnipegfreepress.com/rss/?path=%2Flocal", "http://www.winnipegfreepress.com/rss/?path=%2Fworld"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingsFragment()).commit();

    }

    public void onRadioButtonClicked(View view) {

        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {

        }
    }

    public void setFeedUrl(View view){


    }



   /* @Override
    public void onPause() {
        super.onPause();
        savePreferences("currentUrl", currentURL);
        savePreferences("currentFontSize", currentFontSize);
        savePreferences("currentTheme", currentFontSize);
        savePreferences("pref_syncConnectionType", pref_syncConnectionType);




    }*/



    private void savePreferences(String key, String value){
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_settings, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
/**
 *
 *
 *
 * Settings Fragment Class
 */

    public static class SettingsFragment extends PreferenceFragment implements SharedPreferences.OnSharedPreferenceChangeListener{
        public static final String KEY_PREF_SYNC_CONNECTION_TYPE = "pref_syncConnectionType";
        public static final String KEY_PREF_SYNC_ONOFF = "pref_sync";
        public static final String KEY_PREF_FONT_SIZE = "pref_fontSize";
        public static final String KEY_PREF_FREEPRESS= "http://www.winnipegfreepress.com/rss/?path=";
        public static final String KEY_PREF_DEFAULT_FEED= "pref_defaultFeed";


    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
            Log.d("fired", "shared");
           if (key.equals(KEY_PREF_SYNC_CONNECTION_TYPE)){
               Preference connectionPref = findPreference(key);

               //Set summary to new value
               connectionPref.setSummary(sharedPreferences.getString(key, ""));
           }

            if (key.equals(KEY_PREF_FONT_SIZE)){
                Preference fontPref = findPreference(key);

                //Set summary to new value
                fontPref.setSummary(sharedPreferences.getString(key, ""));
            }

            if (key.equals(KEY_PREF_DEFAULT_FEED)){
                Preference defaultFeedPref = findPreference(KEY_PREF_DEFAULT_FEED);

                //Set summary to new value
                String summary = sharedPreferences.getString(KEY_PREF_DEFAULT_FEED, "local");
                summary = summary.replaceAll("%2F", " ");
                defaultFeedPref.setSummary(summary);
            }

            if (key.equals(KEY_PREF_SYNC_ONOFF)){
                Preference syncEnablePref = findPreference(key);

                 syncEnablePref.setSummary(sharedPreferences.getBoolean(key, true) ? "Enabled": "Disabled");

            }
        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            //Load preferences
            addPreferencesFromResource(R.xml.preferences);
           loadPreferences();
        }

        @Override
        public void onResume() {
            super.onResume();
            getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);

        }

        @Override
        public void onPause() {
            getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
            super.onPause();
        }

        public void loadPreferences(){
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this.getActivity());

            Preference preference = findPreference(KEY_PREF_FONT_SIZE);
            preference.setSummary(sharedPreferences.getString(KEY_PREF_FONT_SIZE, "Medium"));

            preference = findPreference(KEY_PREF_SYNC_ONOFF);
            preference.setSummary(sharedPreferences.getBoolean(KEY_PREF_SYNC_ONOFF, true)? "Enabled" : "Disabled");

            preference = findPreference(KEY_PREF_SYNC_CONNECTION_TYPE);
            preference.setSummary(sharedPreferences.getString(KEY_PREF_SYNC_CONNECTION_TYPE, "Wifi-Only"));

            preference = findPreference(KEY_PREF_DEFAULT_FEED);
            String summary = sharedPreferences.getString(KEY_PREF_DEFAULT_FEED, "local");
            summary = summary.replaceAll("%2F", " ");
            preference.setSummary(summary);

            //listPreference.setPersistent(true);



        }

        @Override
        public boolean onPreferenceTreeClick(PreferenceScreen preferenceScreen, Preference preference) {
            super.onPreferenceTreeClick(preferenceScreen, preference);

            // If the user has clicked on a preference screen, set up the action bar
            if (preference instanceof PreferenceScreen) {
                initializeActionBar((PreferenceScreen) preference);
            }

            return false;
        }

        /** Sets up the action bar for an {@link PreferenceScreen} */
        public static void initializeActionBar(PreferenceScreen preferenceScreen) {
            final Dialog dialog = preferenceScreen.getDialog();

            if (dialog != null) {
                // Inialize the action bar
                dialog.getActionBar().setDisplayHomeAsUpEnabled(true);

                // Apply custom home button area click listener to close the PreferenceScreen because PreferenceScreens are dialogs which swallow
                // events instead of passing to the activity
                // Related Issue: https://code.google.com/p/android/issues/detail?id=4611
                View homeBtn = dialog.findViewById(android.R.id.home);

                if (homeBtn != null) {
                    View.OnClickListener dismissDialogClickListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dialog.dismiss();
                        }
                    };

                    // Prepare yourselves for some hacky programming
                    ViewParent homeBtnContainer = homeBtn.getParent();

                    // The home button is an ImageView inside a FrameLayout
                    if (homeBtnContainer instanceof FrameLayout) {
                        ViewGroup containerParent = (ViewGroup) homeBtnContainer.getParent();

                        if (containerParent instanceof LinearLayout) {
                            // This view also contains the title text, set the whole view as clickable
                            ((LinearLayout) containerParent).setOnClickListener(dismissDialogClickListener);
                        } else {
                            // Just set it on the home button
                            ((FrameLayout) homeBtnContainer).setOnClickListener(dismissDialogClickListener);
                        }
                    } else {
                        // The 'If all else fails' default case
                        homeBtn.setOnClickListener(dismissDialogClickListener);
                    }
                }
            }
        }
    }
}
