package com.desiratech.rssfeedsassignment_final;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseObject;
import com.parse.ParseUser;


public class ShowFullDetails extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_full_details);
        Intent intent = getIntent();
       final String strActivityName =intent.getStringExtra("ActivityName");
       Log.d("activityName", strActivityName);
        final String strTitle = intent.getStringExtra(Welcome.TITLE);
        final String strDescription = intent.getStringExtra(Welcome.DESCRIPTION);
        final String strPubDate = intent.getStringExtra(Welcome.PUBDATE);
        final String strLink = intent.getStringExtra(Welcome.LINK);

        TextView titleView = (TextView) findViewById(R.id.lblFullDetailsTitle);
        TextView pubDateView = (TextView) findViewById(R.id.lblFullDetailsPubDate);
        TextView descriptionView = (TextView) findViewById(R.id.lblFullDetailsDescription);
        TextView linkView = (TextView) findViewById(R.id.lblFullDetailsLink);
        Button saveArticle = (Button) findViewById(R.id.btnSaveArticle);

        titleView.setText("Article Title: " + strTitle);
        pubDateView.setText("Date Published: " + strPubDate);
        descriptionView.setText("Snippet: " + strDescription);

        linkView.setText("Link: "+strLink);
        linkView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openInBrowser(v);
            }
        });

        getActionBar().setDisplayHomeAsUpEnabled(true);

        // Enable Local Datastore.

        saveArticle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveFeedToParse(strLink, strPubDate, strDescription, strTitle);
            }
        });

        if(strActivityName.equals("ViewSavedArticles")){
            saveArticle.setVisibility(View.GONE);
        }
    }


    public void saveFeedToParse(String link, String pubDate, String description, String title){
        ParseObject RSSArticles = new ParseObject("RSS_Articles");
        RSSArticles.put("title", title);
        RSSArticles.put("pubDate", pubDate);
        RSSArticles.put("description", description);
        RSSArticles.put("link", link);
        RSSArticles.put("user", ParseUser.getCurrentUser());

        RSSArticles.saveInBackground();
        Toast.makeText(this, "Article Saved", Toast.LENGTH_SHORT).show();

    }
    public void openInBrowser(View view){
        TextView v = (TextView) view;
        String uRL = v.getText().toString();

        Intent i = new Intent(ShowFullDetails.this, ShowInBrowser.class);
        uRL = uRL.replaceAll("Link: ", "");

        i.putExtra(Welcome.LINK, uRL);

        startActivity(i);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_show_full_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
