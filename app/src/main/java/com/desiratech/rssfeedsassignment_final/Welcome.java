package com.desiratech.rssfeedsassignment_final;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;


public class Welcome extends Activity {

    public final static String TITLE = "com.desiratech.rssfeedsassignment_final.TITLE";
    public final static String DESCRIPTION = "com.desiratech.rssfeedsassignment_final.DESCRIPTION";
    public final static String PUBDATE = "com.desiratech.rssfeedsassignment_final.PUBDATE";
    public final static String LINK = "com.desiratech.rssfeedsassignment_final.LINK";

    public String currentURL, currentFontSize;
    public URL xml_file;
    public BufferedReader in;
    ListView listView;
    private ArrayList<SAXHandler.Entry> entryArrayList = new ArrayList<SAXHandler.Entry>();
    RSSFeeder feedme;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        loadFeed();
        listView = (ListView) findViewById(R.id.rssList);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                sendEntry(view);
            }
        });




    }

    private void loadFeed(){
        if(isInternetAvailable() == 1 || isInternetAvailable() == 2){
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
            currentURL = "http://www.winnipegfreepress.com/rss/?path=" +sharedPreferences.getString("pref_defaultFeed", "%2Flocal").toLowerCase();
            currentFontSize = sharedPreferences.getString("pref_fontSize", "Medium");
            Log.d("Current URL", currentURL);
            feedme = new RSSFeeder();
            feedme.execute();
        } else {
            Toast.makeText(Welcome.this, "Internet Connection: " + NetworkUtil.getConnectivityStatusString(this), Toast.LENGTH_SHORT).show();

        }


    }

    public int isInternetAvailable() {

        return NetworkUtil.getConnectivityStatus(this);

    }



    private void setFontProperties() {

    }


    @Override
    protected void onResume() {
        super.onResume();
        loadFeed();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.action_bar_items, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_settings:
                openSettings();
                return true;

            case R.id.action_saved_articles:
                openSavedArticles();
                return true;

            case R.id.action_logout:
                logUserOut();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void logUserOut(){
        ParseUser.logOut();
        Intent i = new Intent(Welcome.this, LoginSignupActivity.class);
        startActivity(i);
        Toast.makeText(Welcome.this,
                "Logged Out.",
                Toast.LENGTH_LONG).show();
        finish();
    }

    public void openSettings(){
        Intent i = new Intent(this, Settings.class);
        startActivity(i);
    }

    public void openSavedArticles(){
        Intent i = new Intent(this, ViewSavedArticles.class);
        startActivity(i);
    }

    public class RSSFeeder extends AsyncTask<Void, Void, Void> {
        SAXHandler handler;
        @Override
        protected Void doInBackground(Void... params) {
            try {
                xml_file = new URL(currentURL);
                in = new BufferedReader(new InputStreamReader(xml_file.openStream()));

                //yweather:condition
                SAXParserFactory spf = SAXParserFactory.newInstance();
                SAXParser sp = spf.newSAXParser();
                handler = new SAXHandler();

                sp.parse(new InputSource(in), handler);

                //Thread.sleep(10000);
            } catch (MalformedURLException ex) {
                Log.e("MalformedURLException", ex.getMessage());
            } catch (IOException ex) {
                Log.e("IOException", ex.getMessage());
            } catch (SAXException ex) {
                Log.e("SAXException", ex.getMessage());
            } catch (ParserConfigurationException ex) {
                Log.e("ParserConfigurationExc", ex.getMessage());
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            entryArrayList = handler.getArEntries();
            listView.setAdapter(new CustomAdapter(Welcome.this, entryArrayList));

        }

    }

    public void sendEntry(View view){

        Intent intent = new Intent(this, ShowFullDetails.class);

        TextView txtTitle = (TextView) view.findViewById(R.id.lblTitle);
        TextView txtDescription = (TextView) view.findViewById(R.id.lblDescription);
        TextView txtPubDate = (TextView) view.findViewById(R.id.lblPubDate);
        TextView txtLink = (TextView) view.findViewById(R.id.lblLink);


        String strTitle = txtTitle.getText().toString();
        //Log.d("title", strTitle);

        String strDescription = txtDescription.getText().toString();
        String strPubDate = txtPubDate.getText().toString();
        String strLink = txtLink.getText().toString();
        String strActivityName = this.getClass().getSimpleName();

        intent.putExtra("ActivityName", strActivityName);

        intent.putExtra(TITLE, strTitle);
        intent.putExtra(DESCRIPTION, strDescription);
        intent.putExtra(PUBDATE,strPubDate);
        intent.putExtra(LINK,strLink);

        startActivity(intent);
    }
}
